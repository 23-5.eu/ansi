+++
title =  "TOI4465"
tags = ["astronomy", "exoplanets"]
date = "2022-08-09"
+++

### Exoplanet discovery
Back in early August, I had the pleasure to join a team of global amateur astronomers to conduct a [3-day](https://unistellaroptics.com/join-a-three-day-exoplanet-hunt-this-august/)
search for the illusive transit of the planet candidate TOI 4465.01. This gas giant planet candidate is part of the NASA-sponsored long-duration transit-hunting research program at Unistellar. It was discovered with only a single transit, so we had to monitor a 3-day window to try to catch another transit.

There was remarkable participation in this campaign, with Citizen Astronomers around the world providing nearly 230 hours of eVscope observations!

![Timeline](https://static.23-5.eu/ansi/astro/2022-08-09/Exo-TOI4465-Time.png "Timeline")

I am very pleased to announce that the Unistellar Network caught the transit! The transit occurred during the night of September 10 to 11. We had several observers in Europe observing pre-transit baseline, then the US-based observers picked up the observations during transit, and finally, 2 eVscope observations from Japan detected transit egress and post-transit baseline.

![Detection](https://static.23-5.eu/ansi/astro/2022-08-09/lc_toi4465.png "Detection")

The timing here was a bit tricky. As you can see, the transit ingress falls in this little gap between observations. Transit ingress and egress are very important to observe since they “anchor” all of the in-transit data to the out-of-transit data. That gap corresponds to far western Europe (e.g., UK, Spain, Portugal) and the eastern/central North America. However, thankfully,

So the exoplanet candidate TOI 4465.01 is now known as TOI 4465b.

{{< video src="https://unistellaroptics.com/wp-content/uploads/2022/10/Kepler-167e_compressed.mp4" width="666" type="video/mp4" preload="auto" >}}

The press picked it up too.

* [The Register](https://www.theregister.com/2022/09/20/seti_exoplanets_campaign/)
* [Space.com](https://www.space.com/exoplanet-detection-citizen-science-program-tess)
