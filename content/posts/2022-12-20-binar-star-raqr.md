+++
title =  "Astronomy Links"
tags = ["astronomy", "aavso"]
date = "2022-12-20"
draft= "true"
+++

## Introduction
I decided to get my toe into the science part of astronomy. While my beloved eVScope is for observation I
would also like to do more in the field of data analytics. All this is done normally by the SETI scientists when I use my eVScope.

The American Association of Variable Star Observers [AAVSO](https://www.aavso.org) is the best place to start. My eVScope observations are already stored there let's do more science there. After signing up on [iTelescope](https://www.itelescope.net) I have now access to epic telescopes around the globe.
For now, I am reading the [AAVSO Manual](https://www.aavso.org/sites/default/files/publications_files/manual/english_2013/EnglishManual-2013.pdf) and trying the [Easy Stars](https://www.aavso.org/easy-stars).

### R Aquarii
[R Aquarii](https://www.aavso.org/vsots_raqr) is one of these easy ones and is great to observe from Australia. To check the best timing to schedule an observation, I am using
[Stellarium](https://stellarium-web.org). Right now best time is around midnight locale time in Australia.

![R Aquarii](https://upload.wikimedia.org/wikipedia/commons/a/af/Symbiotic_System_R_Aquarii.png "R Aquarii")

### Charts
```
RA:   23:43:49.46
Dec: -15:17:04.2
```

![Star Chart](https://app.aavso.org/vsp/chart/X28265EP.png "Star Chart")

[Variable Star Plotter](https://app.aavso.org/vsp/photometry/?star=R+AQR&fov=60&maglimit=14.5&resolution=150&north=up&east=left)

### iTelescope
https://support.itelescope.net/support/solutions/articles/231920-telescope-33


### Observation Plan
```
#vphot
#count 1,1,1,1,1
#interval 60,120,180,300,600
#binning 1,1,1,1,1
#filter Luminance,Luminance,Luminance,Luminance,Luminance
RAqr	23:43:49.46	-15:17:04.2
#shutdown
```

### Images

### VSPOS