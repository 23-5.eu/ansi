+++
title =  "Astronomy Links"
tags = ["astronomy", "overview"]
date = "2022-12-01"
+++

## Introduction
Hey everyone, I'm passionate about astronomy and have been compiling a list of fantastic websites to delve deeper into this captivating field. Whether you're a seasoned space explorer or just starting your cosmic journey, this list has something for you!

Feel free to browse the links, expand your knowledge, and be amazed by the wonders of the universe. If you stumble upon any other cool astronomy resources, please ping me – I'd love to add them to the list and keep learning together!

* [Earthobservatory](https://earthobservatory.nasa.gov) 20 Years of earth observations on different information with time elapse information on the sensor data
* [Planet.com](https://www.planet.com) Earth observations
* [Popastro](https://www.popastro.com) Online magazine for popular astronomy
* [Astroplaner](https://www.astroplanner.net) Software to plan observations and Astrophotography
* [Las Cumbres Observatory](https://lco.global) Global network of robot telescope for science
* [Cloudynights](https://www.cloudynights.com) Astronomy Forum
* [Strasbourg astronomical Data Center](https://cds.u-strasbg.fr) Online database
* [Astronomerstelegram](https://www.astronomerstelegram.org/) Paper Publishing
* [Supernova Working Group](https://www.wis-tns.org) Online database
* [University of Maryland](https://www.astro.umd.edu) Astronomy news
* [Sciencebooksonline](http://www.sciencebooksonline.info/astronomy.html) List of online Astronomy Books
* [Exoplanet Transit Database](http://var2.astro.cz/ETD/) Czech Astronomical Society
* [Astrobites](https://astrobites.org) Readers Digest for Astro papers
* [Minor Planet Center](https://www.minorplanetcenter.net) Single worldwide location for receipt and distribution of positional measurements of minor planets, comets and outer irregular natural satellites of the major planets
* [Astrometrica](http://www.astrometrica.at/) Interactive software tool for scientific grade astrometric data reduction of CCD images
* [New Object Ephemeris Generator](https://minorplanetcenter.net//iau/MPEph/NewObjEphems.html) Generate Ephemeris based on observations
* [CNEOS](https://cneos.jpl.nasa.gov/) CNEOS is NASA's center for computing asteroid and comet orbits and their odds of Earth impact
* [ToConfirmRA](https://minorplanetcenter.net/iau/NEO/ToConfirmRA.html) List of NEOs to be confirmed
* [FindOrbit](https://www.projectpluto.com/find_orb.htm) Calculate Orbit
* [FIndOrbitOnline](https://www.projectpluto.com/fo.htm) Online Calculate Orbit
* [PrgramITelescope](https://go.itelescope.net/downloads/itn-acp-directives.pdf) Online manual for itelescope and acp commands
* [Fitsblink](http://www.fitsblink.net/residuals/index.html) Calculation of residuals of asteroid positions
* [mapoftheuniverse](https://mapoftheuniverse.net) Explore the universe
* [BAV](https://bav-astro.eu) Bundesdeutsche Arbeitsgemeinschaft für Veränderliche Sterne
* [The Role Of Data Science](https://datascienceprograms.com/learn/the-role-of-data-science-in-astronomy-and-interstellar-exploration/) How data science is revolutionizing space exploration by enabling deeper understanding of the universe and guiding smarter missions. (Thanks to Jeremy from [Students For Research](https://studentsforresearch.org))
* [Astrophysics Data System](https://ui.adsabs.harvard.edu) The SAO/NASA Astrophysics Data System (ADS) is a digital library portal for researchers in astronomy and physics, operated by the [Smithsonian Astrophysical Observatory (SAO)](https://pweb.cfa.harvard.edu/about/about-smithsonian-astrophysical-observatory) under a NASA grant.
* [Archenhold Observatory Citizen Science Dashboard](https://dashboard.astw.de) Dashboard for Citizen Science projects at the [Archenhold Observatory](https://www.astw.de)
* [Jove](https://radiojove.gsfc.nasa.gov) Radio JOVE students and amateur scientists from around the world observe and analyze natural radio emissions of Jupiter, the Sun, and our galaxy using their own easy to construct radio telescopes.
* [Astronomy in Media](https://octaneseating.com/blog/astronomy-in-media/) Great walk through the history of Science Fiction Movies and Science. Thank you Hailey for the link.
* [Navigating the Stars: A Young Learner's Guide to Astronomy] (https://www.snappywords.com/knowledge/navigating-the-stars-a-young-learners-guide-to-astronomy) This guide serves as a compass to understand the vast realm of astronomy, lead you through the cosmos, enhance your understanding of the world, and inspire you to discover your place within this universe. Thanks you Ada for the link.
