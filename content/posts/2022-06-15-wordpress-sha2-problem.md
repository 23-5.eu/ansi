+++
title =  "Fixing the caching_sha2 problem with wordpress and mysql verion 8"
tags = ["docker", "it"]
date = "2022-06-15"
+++

### The problem
I am using wordpress with mysql both in a docker installation. The procedure for my setup is described 
[here](https://web.archive.org/web/20210622005902/https://ansi.23-5.eu/2017/06/wordpress-docker-nginx/). 
Since mysql updated to version 8 they introduced caching_sha2 as the default password algorithm. When you 
use the auto update mechanism in wordpress everything is fine and wordpress still works with the native 
password version configured for the wordpress user. But if you use wordpress in a docker container and 
pull wordpress:latest there is a problem since wordpress 4.9.7 to access the mysql database: (Never thought 
I can use the word wordpress so many times in a sentence!)

```
Warning: mysqli::__construct(): Unexpected server respose while doing caching_sha2 auth: 109 in Standard input code on line 22
Warning: mysqli::__construct(): MySQL server has gone away in Standard input code on line 22
Warning: mysqli::__construct(): (HY000/2006): MySQL server has gone away in Standard input code on line 22
MySQL Connection Error: (2006) MySQL server has gone away
```

### The solution
The solution is relatively easy. You need to change the wordpress user manually from 
​”mysql_native_password” to “caching_sha2_password“. This can be done with a simple SQL call. First 
stop your wordpress docker container and keep the mysql docker container running. Then execute these commands.

```
docker exec -it blog_wordpress_db_1 bash
mysql -u root -pREALLYEPICSECURE
ALTER USER wordpressuser IDENTIFIED WITH caching_sha2_password BY 'REALLYEPICSECURE';
exit
exit
```

Replace blog_wordpress_db_1 with your mysql docker instance name (“docker ps”), “REALLYEPICSECURE” with your 
root password and “wordpressuser” with your wordpress username.

That is basically all. Now you can start your wordpress:latest docker container again and it should work.
