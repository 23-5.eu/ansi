+++
title =  "Ra Shalom"
tags = ["astronomy", "asteroids"]
date = "2022-08-07"
+++

### Near-Earth object Ra Shalom
I participated in the [August 2022 Campaign](https://unistellaroptics.com/citizen-science/planetary-defense/planetary-defense-campaign)
to observe [Ra-Shalom](https://en.wikipedia.org/wiki/2100_Ra-Shalom).
{{< video src="https://static.23-5.eu/ansi/astro/2022-08-07/20220807_Ra-Shalom_kk2_Nennhausen.mp4" width="666" type="video/mp4" preload="auto" >}}
{{< video src="https://static.23-5.eu/ansi/astro/2022-08-07/20220902_Ra-Shalom_kk2_Ahrensfelde.mp4" width="666" type="video/mp4" preload="auto" >}}

The campaign is now over and here is one summary:
![Summary](https://static.23-5.eu/ansi/astro/2022-08-07/campaign-mag.png "Summary")

![Ra Shalom](https://unistellaroptics.com/wp-content/uploads/2022/08/ra_shalom_ss_opt.gif    "Ra Shalom")
![Ra Shalom](https://unistellaroptics.com/wp-content/uploads/2022/08/ra_shalom_stars_opt.gif "Ra Shalom")
