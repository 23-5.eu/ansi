+++
title =  "New Homepage"
tags = ["homepage", "updates"]
date = "2022-06-01"
+++

## Introduction

Starting the (I think fifth) try to build a homepage. Not sure if I convert old stuff or just start from scratch. But this is the first post and more or less a test for hugo. I also plan to setup static information on [23-5.eu](https://www.23-5.eu).
