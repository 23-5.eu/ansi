+++
title =  "Forward to the moon"
tags = ["space", "presentation"]
date = "2022-08-29"
+++

# Forward to the moon
On the occasion of the first start attempt of Artemis 1, I took the liberty and gave a little presentation in our local maker space. The original idea was to catch people with this presentation and get them interested in the space topic. Originally the timing was aligned with the start, so they had some background and could enjoy the launch. As we all know the start was delayed. But nevertheless here is the presentation. Please keep in mind it was ment for everyday audience to get them interested not for already space nerds.

{{< iframe "https://docs.google.com/presentation/d/e/2PACX-1vQg5soAZnxlfjS3JQEpdk1oOk2pX97nes57aeUdCaKsiLT42rSOUN8yVCKSCYn1pDO4VxaZFkxnvtBZ/embed?start=true&loop=false&delayms=3000" >}}

