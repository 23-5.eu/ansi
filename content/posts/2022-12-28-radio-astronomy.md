+++
title =  "Detect arms of our own galaxy"
tags = ["astronomy", "radioastronomy"]
date = "2022-12-28"
+++

### Introduction
Radio astronomy is a branch of astronomy that studies celestial objects and phenomena using radio frequency radiation. One of the key techniques in radio astronomy is the use of neutral hydrogen to detect the structure of our own galaxy, the Milky Way.

Neutral hydrogen is the most abundant element in the universe and is made up of a single proton and a single electron. It is present in virtually all galaxies and is often used to trace the structure of these galaxies. In the Milky Way, neutral hydrogen is found in the interstellar medium, the space between the stars, as well as in the disks and arms of the galaxy.

One of the key ways that neutral hydrogen is detected in radio astronomy is through the use of the 21cm band. This band corresponds to the emission of radio waves at a frequency of 21 cm, which is emitted by the electron in the neutral hydrogen atom as it transitions between two energy levels.

To detect the 21cm emission from neutral hydrogen in the Milky Way, radio telescopes are used to capture the radio waves and convert them into an electrical signal. This signal is then analyzed using specialized software to create a map of the distribution of neutral hydrogen in the galaxy.

The Milky Way is a barred spiral galaxy, which means that it has a central bar-shaped region of stars and gas surrounded by a disk of stars and gas. The disk is further divided into several spiral arms, which are made up of dense concentrations of stars and gas. By mapping the distribution of neutral hydrogen in the galaxy, radio astronomers are able to trace the structure of these arms and gain a better understanding of the overall structure of the Milky Way.

One of the key benefits of using neutral hydrogen to study the Milky Way is that it is relatively easy to detect. Unlike other celestial objects, which may be difficult to see through the dust and gas that fills the galaxy, neutral hydrogen is relatively transparent and can be detected even through these obstacles. This makes it an ideal tracer for studying the structure of the Milky Way.

Another advantage of using neutral hydrogen to study the Milky Way is that it is found throughout the galaxy, including in regions where other types of matter are scarce. This means that neutral hydrogen can be used to trace the structure of the galaxy in places where it is difficult to detect other types of matter, such as in the outer regions of the galaxy or in the halo, the region of diffuse gas and dust that surrounds the galaxy.

In summary, radio astronomy is a powerful tool for studying the structure of the Milky Way and other galaxies. By detecting the 21cm emission from neutral hydrogen, radio astronomers are able to map the distribution of this key element and gain a better understanding of the overall structure of our own galaxy.

You can do so on your own online with the [Pictor Telescope](https://www.pictortelescope.com). Either set the parameters on the web page or us for example the bot on the [Discord Channel "Radio Astronomy and Space](https://discord.gg/Y7rRZs6r).

The antenna is pointing to the zenith so to figure out the best timing for your observation use for example Stellarium either installed on your system or the [online](https://stellarium-web.org) version. Or you can use again the great bot on the Discord Channel.

![Map](https://static.23-5.eu/ansi/astro/2022-12-28/map.png "Map")

## Controll observation
Here is one observation when the antenna is not pointing towards the galaxy disk:
![Nothing](https://static.23-5.eu/ansi/astro/2022-12-28/nothing.png "Nothing")

## Galaxy observation
And here is one when the antenna points to the galaxy itself. You can clearly see the blueshifted hydrogen signal which comes clearly from your galaxy arm.
![Arm](https://static.23-5.eu/ansi/astro/2022-12-28/arm.png "Arm")

## Detection Parameters
```
Your observation has been carried out by PICTOR successfully!
Observation name: blueshift
Observation datetime: 2022-12-15 14:50:40 (UTC+3)
Center frequency: 1420000000.0 Hz
Bandwidth: 2400000 Hz
Sample rate: 2400000 samples/sec
Number of channels: 2048
Number of bins: 100
Observation duration: 150 sec
Observation ID: 58390257
```
