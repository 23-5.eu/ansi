+++
title =  "SDRplay remote on raspberry"
tags = ["HAM", "it", "raspberry"]
date = "2022-08-20"
+++

### SDRPlay
I recently bought myself a [SDRPlay](https://www.sdrplay.com/rsp1a/) receiver to play with this technology and maybe build a 
[ground station](https://satnogs.org/) or [meteor scatter detector](http://www.ars-electromagnetica.de/robs/index.html). 
The original plan is to setup a receiver on the [Motionlab](https://motionlab.berlin) roof with an raspberry pi and send the 
IQ data via network down to a local server and extract the interesting information. One great software to work remotely 
with an SDR receiver is the [Soapy project](https://github.com/pothosware/SoapySDR/wiki).

### Install the raspberry pi part
### Build system
Install the latest raspberry pi lite version from raspberrypi.org

```
sudo apt update
sudo apt upgrade
sudo apt install cmake g++ libpython-dev python-numpy swig git
```

### Core system
The soapy part consist of 3 parts. The Core system must be installed first.

```
git clone https://github.com/pothosware/SoapySDR.git
cd SoapySDR
mkdir build
cd build
cmake ..
make -j4
sudo make install
sudo ldconfig
```

### SDRplay
The SDRplay part consist of two parts one are the proprietary binary libraries from SDRplay itself the the other part is the soapy wrapper for SDRplay.

### Binary Libraries
The driver can be downloaded from the SDRplay homepage [https://www.sdrplay.com/rpi2dl.php](https://www.sdrplay.com/rpi2dl.php)

```
chmod 777 SDRplay_RSP_API-RPi-2.11.1.run
./SDRplay_RSP_API-RPi-2.11.1.run
```

###  The SDRplay Soapy wrapper
``` 
git clone https://github.com/pothosware/SoapySDRPlay.git
cd SoapySDRPlay
mkdir build
cd build
cmake ..
make -j4
sudo make install
```

Test the Soapy access

```
SoapySDRUtil --info
Soapy Server for Remote Access
git clone https://github.com/pothosware/SoapyRemote.git
cd SoapyRemote
mkdir build
cd build
cmake ../ # -DCMAKE_BUILD_TYPE=Debug
make -j4
sudo make install
```

### Run the server
```
SoapySDRServer --bind
```

If you want to run it as a service have a look here on how to autostart stuff in linux.
