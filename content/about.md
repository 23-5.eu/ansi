+++
title = "About"
date = "2022-08-01"
tags = ["page"]
+++

This is my private homepage. Describe some hacks and makes. The content will be presented as blog entries. A static version of my projects can be found [here](https://www.23-5.eu).